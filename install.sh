#!/bin/bash

rsync -a * $HOME/.local/share/gnome-shell/extensions/ \
  --exclude=".git" \
  --exclude="LICENSE" \
  --exclude="README.md" \
  --exclude="install.sh"
