const GLib = imports.gi.GLib;
const ShellToolkit = imports.gi.St;
const Util = imports.misc.util;
const Gio = imports.gi.Gio;
const Main = imports.ui.main;
const FileUtils = imports.misc.fileUtils;

let timer;

// path for scripts of the extensions
var extension_path = "./.local/share/gnome-shell/extensions/sys_monitor/scripts/";
let out;

// Execute the ls command in the scripts directorie of the extension
var cmd = GLib.spawn_sync(extension_path, ["/bin/ls"], null, GLib.SpawnFlags.SEARCH_PATH, null);
// Select the first element of the cmd array, and convert it in string
var scripts_str = new String(cmd[1]);

// Creation of an array of string by split the scripts_str string
var scripts = scripts_str.split("\n");
// Remove the last element because is empty
scripts.pop();

// Iteration paramaters
var it = 0;
var nb_script = scripts.length - 1; // if not -1 iteration error, index out of range


// Main container section
let main_container_properties = { style_class: 'panel-button', reactive: true };
let main_container = new ShellToolkit.Bin(main_container_properties);
let main_container_content = new ShellToolkit.Label({ text: _get_values() });
let main_container_content_updater = function() { main_container_content.set_text(_get_values()); };


function _get_values()
{
	// if you need global var in this function, declare it before main_container section

	var command_output_bytes;
	var command_output_string;

	var current_script_name = scripts[it];
	out = GLib.spawn_sync(extension_path, ["bash",current_script_name,"/"], null, GLib.SpawnFlags.SEARCH_PATH, null);
	command_output_bytes = new String(out[1]);
	//command_output_string = current_script_name.replace(".sh", "") + ": ";

	if (it == nb_script) {
		it = 0;
	} else {
		it += 1;
	}

	//return command_output_string+command_output_bytes;
  return ""+command_output_bytes;
}

function init()
{
	main_container.set_child(main_container_content);
	main_container.connect('button-press-event', main_container_content_updater);
}

function enable()
{
	Main.panel._rightBox.insert_child_at_index(main_container, 0);

	timer = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 5000, () => {
        	main_container_content.set_text(_get_values());
        	return true; // Repeat
	});

}

function disable()
{
	Main.panel._rightBox.remove_child(main_container);
}
