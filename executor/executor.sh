#!/usr/bin/env bash

WORKDIR=$(dirname $0)
INDEX_FILE="$WORKDIR/executor.index"
SCRIPTS_DIR="$WORKDIR/scripts"

for e in $(ls $SCRIPTS_DIR); do
  if [ -x "$SCRIPTS_DIR/$e" ]; then 
    SCRIPTS_LIST=("${SCRIPTS_LIST[@]}" "$e")
  fi
done


if [[ -f $INDEX_FILE ]]; then
  source $INDEX_FILE
  bash $SCRIPTS_DIR/${SCRIPTS_LIST[$INDEX]}
  INDEX=$(( $INDEX + 1 ))

  if [[ $INDEX = ${#SCRIPTS_LIST[@]} ]]; then INDEX=0 ;fi
  echo "INDEX=$INDEX" > $INDEX_FILE
else
  echo "INDEX=0" > $INDEX_FILE
fi
