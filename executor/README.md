## Inspired by
- https://github.com/raujonas/executor
- https://github.com/aaaaadrien/perso-gnome-extension/pull/10#issue-650671264

## How to

1. Put your scripts in the scripts directory (they must be executable)
example:

```
#! /bin/bash

ping -W 1 -4 -c 1 ifconfig.me > /dev/null 2>&1

if [[ "$?" == "0" ]]; then
  WAN_IP=$(curl -s ifconfig.me)
else
  WAN_IP="Not connected"
fi

# Auto detect lan interface based on the default route.
LAN_INTERFACE=$(ip route show default | awk '/default/ {print $5}')

# IPV4 Address
LAN_IP=$(ip a show dev $LAN_INTERFACE | grep --color=none inet | grep -v inet6 | awk '{ print $2; }' )

echo  "LanIP: $LAN_IP     WanIP: $WAN_IP" | tr '\n' ' '
```

2. Call `~/.local/share/gnome-shell/extensions/executor@raujonas.github.io/executor.sh` from executor config window.
